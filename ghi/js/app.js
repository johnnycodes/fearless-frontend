function createCard(
  name,
  description,
  pictureUrl,
  startDate,
  endDate,
  location,
  index
) {
  return `
  <div class="col-4" id="col-${index}">
    <div class="card shadow p-3 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class='card-img-top'>
      <div class="card-body">
        <h5 class='card-title'>${name}</h5>
        <h5 class="card-subtitle mb-2 text-muted">
          ${location}
        </h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
        ${startDate} - ${endDate}
      </div>
    </div>
  </div>
  `;
}

function errorAlert() {
  const alertTag = document.querySelector(".alert");
  alertTag.classList.remove("visually-hidden");
  alertTag.classList.add("alert-danger");
  const message = "there was an error";
  alertTag.innerHTML += message;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      errorAlert();
    } else {
      const data = await response.json();
      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const formattedStartDate = new Date(startDate).toLocaleDateString({
            day: "2-digit",
            month: "2-digit",
            year: "numeric",
          });
          const endDate = details.conference.ends;
          const formattedEndDate = new Date(endDate).toLocaleDateString({
            day: "2-digit",
            month: "2-digit",
            year: "numeric",
          });
          const location = details.conference.location.name;
          const html = createCard(
            name,
            description,
            pictureUrl,
            formattedStartDate,
            formattedEndDate,
            location,
            index
          );
          const cardRow = document.querySelector(".row");
          cardRow.innerHTML += html;
          index += 1;
        } else {
          errorAlert();
        }
      }
    }
  } catch (e) {
    errorAlert();
  }
});
