window.addEventListener("DOMContentLoaded", async () => {
  const confUrl = "http://localhost:8000/api/conferences";
  const response = await fetch(confUrl);
  const selectConference = document.getElementById("conference");
  if (response.ok) {
    const data = await response.json();
    const conferences = data.conferences;
    for (let conference of conferences) {
      const option = document.createElement("option");
      option.value = conference.href;
      option.innerHTML += conference.name;
      selectConference.appendChild(option);
    }
  }

  const formTag = document.getElementById("create-presentation-form");
  formTag.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conference =
      selectConference.options[selectConference.selectedIndex].value;
    const presentationUrl = `http://localhost:8000${conference}presentations/`;
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
    }
  });
});
