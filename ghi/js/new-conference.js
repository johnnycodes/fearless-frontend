window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/";
  const response = await fetch(url);
  const selectLocation = document.getElementById("location");
  if (response.ok) {
    const data = await response.json();
    const locations = data.locations;
    for (let location of locations) {
      const optionLocation = document.createElement("option");
      optionLocation.setAttribute("value", location.id);
      optionLocation.innerHTML = `${location.name}`;
      selectLocation.appendChild(optionLocation);
    }
  }
  const formTag = document.getElementById("create-conference-form");
  formTag.addEventListener("submit", async (e) => {
    e.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
    }
  });
});
