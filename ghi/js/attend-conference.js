window.addEventListener("DOMContentLoaded", async () => {
  const conferenceUrl = "http://localhost:8000/api/conferences/";
  const response = await fetch(conferenceUrl);
  const selectTag = document.getElementById("conference");
  if (response.ok) {
    const data = await response.json();
    const conferences = data.conferences;
    console.log(conferences);
    for (let conference of conferences) {
      const option = document.createElement("option");
      option.innerHTML = conference.name;
      option.value = conference.href;
      selectTag.appendChild(option);
    }
    const selectConfSpinner = document.getElementById(
      "loading-conference-spinner"
    );
    selectConfSpinner.classList.add("d-none");
    selectTag.classList.remove("d-none");
  }
  const formTag = document.getElementById("create-attendee-form");
  formTag.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const attendeeUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      const successMessage = document.getElementById("success-message");
      successMessage.classList.add("d-none");
      formTag.classList.remove("d-none");
      formTag.reset();
    }
  });
});
