import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ShowConferenceDetails from "./ShowConferenceDetails";
import AppRouter from "../../AppRouter";

export default function ShowConferences() {
  const listConfsUrl = "http://localhost:8000/api/conferences";
  const baseUrl = "http://localhost:8000";
  const [conferenceList, setConferenceList] = useState([]);
  const [conferenceDetails, setConferenceDetails] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(listConfsUrl);
        const json = await response.json();
        const conferences = json.conferences;
        setConferenceList(conferences);
        console.log("data finished");
      } catch (error) {
        console.log("error", error);
      }
    };
    fetchData();
  }, []);

  const fetchDetails = (e, props) => {
    <ShowConferenceDetails props={props} />;
    // const combinedUrl = `${baseUrl}${props}`;
    // try {
    //   const response = await fetch(combinedUrl);
    //   const json = await response.json();
    //   const dets = json.conference;
    //   setConferenceDetails((prevDets) => [...prevDets, dets]);
    //   console.log("details finished", dets);
    // } catch (error) {
    //   console.log("error", error);
    // }
  };

  return (
    <>
      {conferenceList &&
        conferenceList.map((conference) => (
          <a
            key={conference.href}
            href="/"
            onClick={() => {
              return <ShowConferenceDetails conference />;
            }}
          >
            <p>{conference.name}</p>
          </a>
        ))}

      <div>{conferenceDetails.name}</div>
    </>
  );
}
