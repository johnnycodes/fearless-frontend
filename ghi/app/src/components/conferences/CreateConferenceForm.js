import React, { useState, useEffect } from "react";

function CreateConferenceForm() {
  const [name, setName] = useState([]);
  const [startDate, setStartDate] = useState([]);
  const [endDate, setEndDate] = useState([]);
  const [description, setDescription] = useState([]);
  const [maxPresentations, setMaxPresentations] = useState([]);
  const [maxAttendees, setMaxAttendees] = useState([]);
  const [location, setLocation] = useState([]);
  const [allLocations, setAllLocations] = useState([]);

  const nameChange = (e) => {
    const data = e.target.value;
    setName(data);
  };
  const startChange = (e) => {
    const data = e.target.value;
    setStartDate(data);
  };

  const endChange = (e) => {
    const data = e.target.value;
    setEndDate(data);
  };

  const descChange = (e) => {
    const data = e.target.value;
    setDescription(data);
  };

  const maxPresChange = (e) => {
    const data = e.target.value;
    setMaxPresentations(data);
  };

  const changeLoc = (e) => {
    const data = e.target.value;
    setLocation(data);
  };

  const maxAttChange = (e) => {
    const data = e.target.value;
    setMaxAttendees(data);
  };

  const getLocations = async () => {
    const locationUrl = "http://localhost:8000/api/locations/";
    const response = await fetch(locationUrl);
    if (response.ok) {
      const data = await response.json();
      const locations = data.locations;
      setAllLocations(locations);
    }
  };

  useEffect(() => {
    getLocations();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {};
    data.name = name;
    data.starts = startDate;
    data.ends = endDate;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = location;

    const json = JSON.stringify(data);
    const url = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
    } catch (error) {
      console.log("error", error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                  onChange={nameChange}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Start Date"
                  required
                  type="date"
                  name="starts"
                  id="starts"
                  className="form-control"
                  onChange={startChange}
                />
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="End Date"
                  required
                  type="date"
                  name="ends"
                  id="ends"
                  className="form-control"
                  onChange={endChange}
                />
                <label htmlFor="ends">End Date</label>
              </div>
              <div className="form-control mb-3">
                <textarea
                  placeholder="Description of Conference"
                  required
                  name="description"
                  id="description"
                  className="form-control"
                  onChange={descChange}
                ></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="0"
                  required
                  type="number"
                  name="max_presentations"
                  id="max_presentations"
                  className="form-control"
                  onChange={maxPresChange}
                />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  type="number"
                  placeholder="Max Attendees"
                  required
                  name="max_attendees"
                  id="max_attendees"
                  className="form-control"
                  onChange={maxAttChange}
                />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>

              <div className="form-floating mb-3">
                <select
                  name="location"
                  id="location"
                  className="form-select"
                  onChange={changeLoc}
                >
                  <option value="">Choose a Location</option>
                  {allLocations.map((location) => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary" value="save">
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateConferenceForm;
