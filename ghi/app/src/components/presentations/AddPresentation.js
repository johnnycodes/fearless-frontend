import React, { useState, useEffect } from "react";

function AddPresentation() {
  const [presenterName, setPresenterName] = useState("");
  const [presenterEmail, setPresenterEmail] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conferenceHREF, setConferenceHREF] = useState("");
  const [conferenceName, setConferenceName] = useState([]);
  const [allConferences, setAllConferences] = useState([]);

  const presenterNameChange = (e) => {
    const data = e.target.value;
    setPresenterName(data);
  };

  const presenterEmailChange = (e) => {
    const data = e.target.value;
    setPresenterEmail(data);
  };

  const titleChange = (e) => {
    const data = e.target.value;
    setTitle(data);
  };

  const synopsisChange = (e) => {
    const data = e.target.value;
    setSynopsis(data);
  };

  const selectLocationChange = (e) => {
    const href = e.target.value;
    setConferenceHREF(href);
    const name = document.getElementById(href);
    const encodedName = name.innerHTML;
    const tempElement = document.createElement("div");
    tempElement.innerHTML = encodedName;
    const decodedName = tempElement.textContent;
    setConferenceName(decodedName);
  };
  const getConferences = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAllConferences(data.conferences);
    }
  };

  useEffect(() => {
    getConferences();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    const conference = {};
    data.presenter_name = presenterName;
    data.presenter_email = presenterEmail;
    data.title = title;
    data.synopsis = synopsis;
    conference.href = conferenceHREF;
    conference.name = conferenceName;
    data.conference = conference;
    const json = JSON.stringify(data);
    const presentationUrl = `http://localhost:8000${conferenceHREF}presentations/`;
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(presentationUrl, fetchConfig);
    } catch (error) {
      console.log("error posting presentation", error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form id="create-presentation-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Name"
                  required
                  type="text"
                  name="presenter_name"
                  id="presenter_name"
                  className="form-control"
                  onChange={presenterNameChange}
                />
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  type="text"
                  required
                  name="presenter_email"
                  id="presenter_email"
                  placeholder="Email"
                  className="form-control"
                  onChange={presenterEmailChange}
                />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                  className="form-control"
                  onChange={titleChange}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  placeholder="Synopsis"
                  required
                  name="synopsis"
                  id="synopsis"
                  className="form-control"
                  onChange={synopsisChange}
                />
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="form-floating mb-3">
                <select
                  required
                  name="conference"
                  id="conference"
                  className="form-control"
                  onChange={selectLocationChange}
                >
                  <option value="">Select a Conference</option>
                  {allConferences.map((conference) => {
                    return (
                      <option
                        key={conference.href}
                        id={conference.href}
                        value={conference.href}
                      >
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary" value="save">
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddPresentation;
