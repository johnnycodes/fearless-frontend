function CreateAccountForm() {
  const formTag = document.getElementById("create-account-form");
  async function handleSubmit(e) {
    e.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
    const signUpUrl = "http://localhost:8000/api/accounts/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(signUpUrl, fetchConfig);
      formTag.reset();
    } catch {
      console.log("error");
    }
  }
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="create-account-header">
            <h1>Create an Account</h1>
          </div>
          <form id="create-account-form">
            <div className="form-floating mb-3">
              <input
                type="text"
                id="username"
                name="username"
                className="form-control"
                placeholder="Username: "
              />
              <label htmlFor="username">Username: </label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Email:"
                type="text"
                id="email-sign-up"
                name="email"
                className="form-control"
              />
              <label htmlFor="email">Email:</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Password:"
                type="password"
                id="password"
                name="password"
                className="form-control"
              />
              <label htmlFor="password">Password: </label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="First Name:"
                type="text"
                id="first_name"
                name="first_name"
                className="form-control"
              />
              <label htmlFor="first_name">First Name: </label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Last Name:"
                type="text"
                id="last_name"
                name="last_name"
                className="form-control"
              />
              <label htmlFor="last_name">Last Name: </label>
            </div>
            <div className="form-floating mb-3">
              <button onClick={handleSubmit}>Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateAccountForm;
