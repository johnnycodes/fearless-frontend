import React, { useState, useEffect } from "react";

function LoginForm() {
  const [username, setUsername] = useState([]);
  const [password, setPassword] = useState([]);

  const usernameChange = (e) => {
    const data = e.target.value;
    setUsername(data);
  };

  const passwordChange = (e) => {
    const data = e.target.value;
    setPassword(data);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.username = username;
    data.password = password;
    const json = JSON.stringify(data);
    console.log("json", json);
    const fetchOptions = {
      method: "post",
      body: json,
      credentials: "include",
    };
    const url = "http://localhost:8000/login/";
    try {
      const response = await fetch(url, fetchOptions);
    } catch (error) {
      console.log("error logging in", error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Login</h1>
            <form id="login-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Username"
                  required
                  type="text"
                  name="username"
                  id="username"
                  className="form-control"
                  onChange={usernameChange}
                />
                <label htmlFor="username">Username</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Password"
                  required
                  type="password"
                  name="password"
                  id="password"
                  className="form-control"
                  onChange={passwordChange}
                />
                <label htmlFor="password">Password</label>
              </div>
              <button className="btn btn-primary">Login</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginForm;
