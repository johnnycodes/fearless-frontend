import React, { useState, useEffect } from "react";

function LocationForm() {
  const fetchData = async () => {
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const states = data.states;
      const selectTag = document.getElementById("state");
      for (let key in states) {
        const option = document.createElement("option");
        option.value = states[key];
        option.innerHTML += key;
        selectTag.appendChild(option);
      }
    }
  };
  const [name, setName] = useState([]);
  const nameChange = (e) => {
    const data = e.target.value;
    setName(data);
  };
  const [city, setCity] = useState([]);
  const cityChange = (e) => {
    const data = e.target.value;
    setCity(data);
  };
  const [roomCount, setRoomCount] = useState([]);
  const roomCountChange = (e) => {
    const data = e.target.value;
    setRoomCount(data);
  };
  const [selState, setSelState] = useState([]);
  const selStateChange = (e) => {
    const data = e.target.value;
    setSelState(data);
  };

  useEffect(() => {
    fetchData();
    console.log();
  }, []);
  const formTag = document.getElementById("create-location-form");
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.name = name;
    data.city = city;
    data.room_count = roomCount;
    data.state = selState;
    const json = JSON.stringify(data);
    console.log(data);
    const url = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      formTag.reset();
    } catch (error) {
      console.log("error", error);
    }
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div class="offset-3 col-6">
            <div class="shadow p-4 mt-4">
              <h1>Create a New Location</h1>
              <form id="create-location-form" onSubmit={handleSubmit}>
                <div class="form-floating mb-3">
                  <label htmlFor="name">Location: </label>
                  <input
                    class="form-control"
                    type="text"
                    name="name"
                    id="name"
                    onChange={nameChange}
                  />
                </div>
                <div class="form-floating mb-3">
                  <label htmlFor="city">City</label>
                  <input
                    class="form-control"
                    type="text"
                    name="city"
                    id="city"
                    onChange={cityChange}
                  />
                </div>
                <div class="form-floating mb-3">
                  <label htmlFor="room_count">Room Count</label>
                  <input
                    type="number"
                    name="room_count"
                    id="room_count"
                    onChange={roomCountChange}
                    class="form-control"
                  />
                </div>
                <div class="form-floating mb-3">
                  <select
                    id="state"
                    name="state"
                    onChange={selStateChange}
                    className="form-select"
                  >
                    <option value="">Select a State</option>
                  </select>
                </div>

                <div>
                  <button class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default LocationForm;
