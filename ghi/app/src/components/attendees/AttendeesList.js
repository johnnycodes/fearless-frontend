import AttendeeSignUpForm from "./AttendeeSignUpForm";

async function handleDetails(attendee) {
  const attendeeDetailUrl = attendee.attendee.href;
  const detailUrl = "http://localhost:8001" + attendeeDetailUrl;
  const attendeeHiddenTr = document.getElementById(`${attendee.attendee.href}`);
  const attendeeHiddenEmail = attendeeHiddenTr.firstChild;
  const attendeeHiddenHasAccount =
    attendeeHiddenTr.getElementsByTagName("td")[1];
  const attendeeHiddenLast = attendeeHiddenTr.getElementsByTagName("td")[2];
  try {
    const response = await fetch(detailUrl);
    const data = await response.json();

    if (attendeeHiddenTr.classList.contains("tr-hidden")) {
      attendeeHiddenTr.classList.remove("tr-hidden");
      if (!attendeeHiddenTr.classList.contains("clicked")) {
        attendeeHiddenEmail.innerHTML += data.email;
        attendeeHiddenHasAccount.innerHTML += data.has_account;
        attendeeHiddenLast.innerHTML += data.created;
      } else {
        return;
      }
    } else {
      attendeeHiddenTr.classList.add("tr-hidden");
      attendeeHiddenTr.classList.add("clicked");
    }
  } catch (error) {}
}

function AttendeesList(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <div className="table-container">
      <table className="table table-striped table-light">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
            <th>Details</th>
          </tr>
        </thead>
        <tbody>
          {props.attendees.map((attendee) => {
            return (
              <>
                <tr key={attendee.href}>
                  <td>{attendee.name}</td>
                  <td>{attendee.conference}</td>
                  <td>
                    <button onClick={() => handleDetails({ attendee })}>
                      Details
                    </button>
                  </td>
                </tr>
                <tr
                  className="extra-details tr-hidden"
                  key={attendee.name}
                  id={attendee.href}
                >
                  <td key="email">Email: </td>
                  <td key="account">Has Account: </td>
                  <td key="created"></td>
                </tr>
              </>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AttendeesList;
