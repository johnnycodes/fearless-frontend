import { useState, useEffect } from "react";

function AttendeeSignUpForm() {
  const confUrl = "http://localhost:8000/api/conferences/";
  const [conferences, setConferences] = useState([]);
  const fetchData = async () => {
    try {
      const response = await fetch(confUrl);
      const json = await response.json();
      const conferences = json.conferences;
      setConferences(conferences);
    } catch (error) {
      console.log("error", error);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const [name, setName] = useState("");
  const handleNameChange = (e) => {
    const value = e.target.value;
    setName(value);
  };

  const [email, setEmail] = useState("enter email");
  const handleEmailChange = (e) => {
    const value = e.target.value;
    setEmail(value);
  };

  const [conference, setConference] = useState("");
  const handleConferenceChange = (e) => {
    const value = e.target.value;
    setConference(value);
  };

  const formTag = document.getElementById("create-attendee-form");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};

    data.name = name;
    data.email = email;
    data.conference = conference;

    const json = JSON.stringify(data);
    const attendeeConfUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "POST",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(attendeeConfUrl, fetchConfig);
      formTag.reset();
    } catch {
      console.log("catch error");
    }
  };

  return (
    <div className="container">
      <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <img
              width="300"
              className="bg-white rounded shadow d-block mx-auto mb-4"
              src="logo.svg"
              alt="logo"
            />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form id="create-attendee-form" onSubmit={handleSubmit}>
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference you'd like to attend.
                  </p>
                  <div className="mb-3">
                    <select
                      name="conference"
                      id="conference"
                      className="form-select"
                      required
                      onChange={handleConferenceChange}
                    >
                      <option value="">Select a Conference</option>
                      {conferences &&
                        conferences.map((conference) => (
                          <option
                            key={conference.href}
                            id={conference.href}
                            value={conference.href}
                          >
                            {conference.name}
                          </option>
                        ))}
                    </select>
                  </div>
                  <p className="mb-3">Now, tell us about yourself.</p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          required
                          placeholder="Your full name"
                          type="text"
                          id="name"
                          name="name"
                          className="form-control"
                          onChange={handleNameChange}
                        />
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          required
                          placeholder="Your email address"
                          type="email"
                          id="email"
                          name="email"
                          className="form-control"
                          onChange={handleEmailChange}
                        />
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                <div
                  className="alert alert-success d-none mb-0"
                  id="success-message"
                >
                  Congratulations! You're all signed up!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendeeSignUpForm;
