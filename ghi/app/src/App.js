import "./App.css";
import Nav from "./Nav";
import AppRouter from "./AppRouter";

function App(props) {
  return (
    <div className="App">
      <Nav />
      <AppRouter attendees={props.attendees} />
    </div>
  );
}

export default App;
