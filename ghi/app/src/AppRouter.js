import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AttendeeSignUpForm from "./components/attendees/AttendeeSignUpForm";
import AttendeesList from "./components/attendees/AttendeesList";
import CreateAccountForm from "./components/accounts/CreateAccountForm";
import ShowConferences from "./components/conferences/ShowConferences";
import ShowConferenceDetails from "./components/conferences/ShowConferenceDetails";
import LocationForm from "./components/locations/LocationForm";
import CreateConferenceForm from "./components/conferences/CreateConferenceForm";
import AddPresentation from "./components/presentations/AddPresentation";
import LoginForm from "./components/accounts/LoginForm";

function AppRouter(props) {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<ShowConferences />} />
        <Route path="/conference/:name" element={<ShowConferenceDetails />} />
        <Route
          path="/attendees"
          element={<AttendeesList attendees={props.attendees} />}
        />
        <Route path="/create-account" element={<CreateAccountForm />} />
        <Route path="/attendee-sign-up" element={<AttendeeSignUpForm />} />
        <Route path="/create-location" element={<LocationForm />} />
        <Route path="/create-conference" element={<CreateConferenceForm />} />
        <Route path="/add-presentation" element={<AddPresentation />} />
        <Route path="/login" element={<LoginForm />} />
      </Routes>
    </Router>
  );
}

export default AppRouter;
